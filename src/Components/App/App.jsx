import React from "react";
import Dummy from '../DummyPage/Dummy';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import LoginForm from "../EntryForms/LoginForm";
import RegistryForm from "../EntryForms/RegistryForm";
import './App.css';
import TablePage from '../TablePage/TablePage';
import UserEditModal from "../MainTable/UserEditModal/UserEditModal";

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path='/'>
            <Dummy/>
          </Route>
          <Route path='/register'>
            <RegistryForm/>
          </Route>
          <Route path='/login'>
            <LoginForm/>
          </Route>
          <Route path='/table'>
            <TablePage/>
          </Route>
          <Route path='/edit'>
            <UserEditModal/>
          </Route>
        </Switch>
      </div>
    </Router>
    
  );
}

export default App;
