import React from 'react';
import { ListGroup } from 'react-bootstrap';
import MainTableRow from './MainTableRow';


const MainTableBody = (props) => {
    return (
        <ListGroup>
            {props.users.map(user => (
                <MainTableRow key={user.id} id={user.id} email={user.email} password={user.password} tenantID={user.tenantID}/>
            ))}
        </ListGroup>
    )
}

export default MainTableBody;