import React, { useState } from "react";
import MainTable from "../MainTable/MainTable";
import SearchRow from "../SearchRow/SearchRow";

const TablePage = () => {

    const productsGenerator = quantity => {
        const items = [];
        for (let i = 0; i < quantity; i++) {
          items.push({ id: i, email: 'mail' + i + '@mail.com', password: i, tenantID: 'tenant' + i });
        }
        return items;
    };

    const users = productsGenerator(50);

    const [search, setSearch] = useState('')

    const handleSearchSubmit = (event) => {
        setSearch(event.target.value.toLowerCase());
    }

    return (
        <div className='page-table'>
            <SearchRow handleSearchSubmit={handleSearchSubmit}/>
            <MainTable users={users} search={search}/>
        </div>
    );
}

export default TablePage;