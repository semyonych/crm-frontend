import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './UserEditModal.css';

const UserEditModal = (props) => {
    return (
        <Modal show={props.show} onHide={props.handleShow} centered className='user-edit-modal'>
            <Modal.Header>
                <Modal.Title>User account</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>User ID:</Form.Label>
                        <Form.Control type="plaintext" readOnly defaultValue={props.id} className="form__readOnly"/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Tenant ID:</Form.Label>
                        <Form.Control type="plaintext" readOnly defaultValue={props.tenantID} className="form__readOnly"/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Role:</Form.Label>
                        <Form.Control type="plaintext" readOnly defaultValue="Client" className="form__readOnly"/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address:</Form.Label>
                        <Form.Control type="email" defaultValue={props.email} className="form__input"/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password" defaultValue={props.password} className="form__input"/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Link to='/table'>
                    <Button variant="secondary" className='form__btn-back' onClick={props.handleShow}>Cancel</Button>
                </Link>
                <Button variant="primary" className='form__btn-save'>Save changes</Button>
            </Modal.Footer>
        </Modal>
        
    );
}

export default UserEditModal;