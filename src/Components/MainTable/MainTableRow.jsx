import React, { useState } from "react";
import './MainTable.css'
import { Button, ListGroupItem } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import UserEditModal from "./UserEditModal/UserEditModal";
 
const MainTableRow = (props) => {
    const [showModal, setShowModal] = useState(false);

    function handleShow() {
        setShowModal(!showModal);
    }
    
    return (
        <ListGroupItem className='d-flex justify-content-between single-user'>
            <div className='single-user__id single-user__block'>{props.id}</div>
            <div className='single-user__email single-user__block'>{props.email}</div>
            <div className='single-user__pass single-user__block'>{props.password}</div>
            <div className='single-user__tenant-id single-user__block'>{props.tenantID}</div>
            <div className=' single-user__block single-user__button-block'>
                {/* <Link to='/edit'> */}
                    <Button 
                        className='single-user__edit-button'
                        onClick={handleShow}
                    >
                        Edit</Button>
                {/* </Link> */}
            </div>
            <UserEditModal 
                show={showModal} 
                id={props.id} 
                email={props.email} 
                password={props.password}
                tenantID={props.tenantID}
                handleShow={handleShow}
            />
        </ListGroupItem>
    );
}

export default MainTableRow;