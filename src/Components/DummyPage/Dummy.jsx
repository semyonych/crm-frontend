import React from "react";
import './Dummy.css';
import { Container, Row, Col, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';

const Dummy = () => {
    return (
        <Container className='dummy-page'>
            <Row>
                <Col>
                    <div className='dummy-buttons'>
                        <Link to='/register'>
                            <Button variant='primary' size='lg' className='dummy-buttons__button'>Register</Button>
                        </Link>
                        <Link to='/login'>
                            <Button variant='primary' size='lg' className='dummy-buttons__button'>Sing In</Button>
                        </Link>
                        <Link to='/table'>
                            <Button variant='primary' size='lg' className='dummy-buttons__button'>Table</Button>
                        </Link>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default Dummy;