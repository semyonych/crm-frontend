import React, { useState } from "react";
import './forms.css';
import { Form, Button } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const emailChange = (event) => {
        setEmail(event.target.value);
    };

    const passwordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const user = {
            email: email,
            password: password,
            tenantId: 'd0772378-7c87-44f1-aa2a-0cc1c045a821'
        };
        var raw = JSON.stringify(user);
        var requestOptions = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: raw,
        redirect: 'follow',
        };

        fetch('http://localhost:8000/api/v1/auth/signin', requestOptions)
        .then(response => response.status === 200 ? history.push('/table') :  console.log(response.text()))
        .catch(error => console.log('error', error));  
    };

    
    
    

    return (
        <div>
            <Form className='form' onSubmit={handleSubmit}>
                <Form.Group className='form__line'>
                    <Form.Label className='line__name'>Email</Form.Label>
                    <Form.Control 
                        type='text' 
                        // value={user.email} 
                        placeholder='Enter email' 
                        className='line__input'
                        onChange={emailChange}
                    ></Form.Control>
                </Form.Group>
                <Form.Group className='form__line'>
                    <Form.Label className='line__name'>Password</Form.Label>
                    <Form.Control 
                        type='password' 
                        // value={user.password} 
                        placeholder='********' 
                        className='line__input'
                        onChange={passwordChange}
                    ></Form.Control>
                </Form.Group>
                <Button 
                    type='submit' 
                    variant='primary' 
                    className='form__button'
                >
                    Login
                </Button>
                <Link to='/'>
                    <Button 
                        variant='primary' 
                        className='form__button'
                    >
                        Back
                    </Button>
                </Link>
            </Form>
            
        </div>
    );
}

export default LoginForm;