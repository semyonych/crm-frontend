import Button from "@restart/ui/esm/Button";
import React from "react";
import { Container, Row, Col, InputGroup, FormControl } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "./SearchRow.css"

const SearchRow = (props) => {
    return (
        <Container className='search-container'>
            <Row className='search-row'>
                <Col className='button-holder'>
                    <Link to='/'>
                        <Button variant='primary'>Home</Button>
                    </Link>
                </Col>
                <Col className='input-holder'>
                    <InputGroup>
                        <FormControl
                            className='input-holder__input-field'
                            placeholder='Search'
                            aria-label='Search'
                            aria-describedby='search-button'
                            onChange={props.handleSearchSubmit}
                        />
                    </InputGroup>
                </Col>
            </Row>
        </Container>
    );
}

export default SearchRow;