import React, { useState, useEffect } from "react";
import { Container, Row, Col, ListGroup, ListGroupItem, } from 'react-bootstrap';
import './MainTable.css'
import MainTableBody from "./MainTableBody";
import Pagination from "./Pagination";




const MainTable = (props) => {

   

    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(10);
    const [searchResult, setSearchResult] = useState([]);
    const users = props.users;
    const search = props.search;

    useEffect(() => {
        const results = users.filter(user =>
            JSON.stringify(user).toLowerCase().includes(search)
        );
        setSearchResult(results);
    }, [search, users]);

    // Get current posts
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentUsers = searchResult.slice(indexOfFirstPost, indexOfLastPost);

    // Change page
    const paginate = pageNumber => setCurrentPage(pageNumber);

    return (
        <Container className='users-table-wrapper'>
            <Row>
                <Col>
                    <ListGroup>
                        <ListGroupItem className='user-table__labels d-flex justify-content-between'>
                            <div className='labels__label'>ID</div>
                            <div className='labels__label'>Email</div>
                            <div className='labels__label'>Password</div>
                            <div className='labels__label'>Tenant ID</div>
                            <div className='labels__label'></div>
                        </ListGroupItem>
                    </ListGroup>
                    <MainTableBody users={currentUsers}/>
                    <Pagination
                        postsPerPage={postsPerPage}
                        totalPosts={searchResult.length}
                        paginate={paginate}
                    />
                </Col>
            </Row>
            
        </Container>
        
    );
}

export default MainTable;