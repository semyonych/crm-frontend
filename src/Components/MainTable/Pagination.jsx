import React from 'react';
// import Dropdown from 'react-bootstrap/Dropdown';

const Pagination = ({ postsPerPage, totalPosts, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className='d-flex justify-content-end'>
      {/* <Dropdown>
        <Dropdown.Toggle>{props.postsPerPage}</Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item onClick={props.setPostsPerPage(10)}>10</Dropdown.Item>
          <Dropdown.Item>25</Dropdown.Item>
          <Dropdown.Item>50</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown> */}
      <ul className='pagination'>
        {pageNumbers.map(number => (
          <li key={number} className='page-item'>
            <a onClick={(event) => {paginate(number); event.preventDefault()} } href='!#' className='page-link'>
              {number}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Pagination;
